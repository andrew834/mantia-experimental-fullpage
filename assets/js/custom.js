var fadeSpeed = 'fast';

$(document).ready(function() {
    checkScroll();
    startVideo();
    $(window).scroll(function() {
        checkScroll();
    });
});

function checkScroll(){    
    if ($(document).scrollTop() < 100) {
        primaryNavigation();
    }else if($(document).scrollTop() > 100 && $(document).scrollTop() < 600){
        $('#main-nav').fadeOut(fadeSpeed);    
    }else if($(document).scrollTop() > 600){
        secondaryNavigation();
    }
}

function startVideo(){
    //document.getElementById('frontpage-vid').play();
}

function primaryNavigation(){
    setMenuColor("white");
    $('#main-nav').css('box-shadow','0 0 10px 0 rgba(0,0,0,.0)');
    $('#main-nav').css('background-color', "transparent");
    $('#main-nav').fadeIn(fadeSpeed);
}

function secondaryNavigation(){
    setMenuColor("black");
    $('#main-nav').css('box-shadow','0 0 10px 0 rgba(0,0,0,.1)');
    $('#main-nav').css('background-color', "#F7F6F1");
    $('#main-nav').fadeIn(fadeSpeed);
    $('.nav-link').css('color', 'red !important');
}

function setMenuColor(color){
    
    if(color === "white"){
        $('.custom-navbar-toggler').removeClass('navbar-light');
        $('.custom-navbar-toggler').addClass('navbar-dark');
        $('#nav-logo-black').css('display','none');
        $('#nav-logo-white').css('display','block');
    }else{
        $('.custom-navbar-toggler').removeClass('navbar-dark');
        $('.custom-navbar-toggler').addClass('navbar-light');
        $('#nav-logo-white').css('display','none');
        $('#nav-logo-black').css('display','block');
    }
    
    $("#nav-whiskey").css("color", color);
    $("#nav-story").css("color", color);
    $("#nav-findus").css("color", color);
    $("#nav-merch").css("color", color);

    $("#social-instagram").css("color", color);
    $("#social-twitter").css("color", color);
    $("#social-facebook").css("color", color);
    $("#social-pintrest").css("color", color);
}